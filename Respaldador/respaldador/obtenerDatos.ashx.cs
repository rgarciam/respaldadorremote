﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Text;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using Ionic.Zip;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Runtime.Remoting.Messaging;

namespace respaldador
{
    /// <summary>
    /// Descripción breve de obtenerDatos
    /// </summary>
    public class obtenerDatos : IHttpHandler
    {

        /// <summary>
        /// Función para consumir la función respaldador.
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            var parameters = context.Request.QueryString;
            var tabla = parameters[0];
            var campo = parameters[1];
            var tipo = parameters[2];
            var llaves = parameters[3];
            var rutaArchivo = parameters[4]; //@"C:\cienciamed\csv";
            var nombreArchivo = parameters[5];
            var limpiarRegistros = bool.Parse(parameters[6]);
            var sobreescribirarchivo = bool.Parse(parameters[7]);
            var guardarTitulos = bool.Parse(parameters[8]);

            _respaldador(tabla, campo, tipo, llaves, rutaArchivo, nombreArchivo, limpiarRegistros, sobreescribirarchivo, guardarTitulos);

            //_restaurador("cienciamedCSV", "id", "int", "1-764560", ruta, nombre, false, ",");
            //_restaurador("cienciamedCSV", "campoDate", "date", "20170525-20170526", ruta, nombre, false, ",");
            //_restaurador("cienciamedCSV", "campoBit", "bit", "1", ruta, nombre, false, ",");
            //_restaurador("cienciamedCSV", "campoDatetime", "datetime", "20170527,20170528-20170529", ruta, nombre, false, ",");
            //20170525,20170527-20170529
            nombreArchivo = @"archivo07-3.zip";
            _restaurador(tabla, campo, tipo, llaves, rutaArchivo, nombreArchivo, false, ",");

            context.Response.Write(JsonConvert.SerializeObject(new
            {
                data = "true"
            }));
        }

        /// <summary>
        /// Función para restaurar hacia una tabla temporal y filtrar registros contenidos en un csv para posteriormente generar 
        /// los datos en una tabla física.
        /// Si el párametro sql, es false entonces se filtra directamente desde el archivo csv para luego ingresar los datos en
        /// la tabla física.
        /// </summary>
        /// <param name="tablaRestaurar"></param>
        /// <param name="campo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sobreescribirBaseDatos"></param>
        /// <param name="separador"></param>
        public void _restaurador(string tablaRestaurar, string campo, string tipoDato, string llavesRestaurar, string rutaArchivo, string nombreArchivo,
            bool sobreescribirBaseDatos, string separador)
        {

            //Se valida que la tabla exista
            if (!TablaExiste(tablaRestaurar)) return;

            //Se valida que el campo exista
            if (!CampoExiste(tablaRestaurar, campo)) return;

            //Se valida Tipo de Dato
            ValidaTipoDato(tipoDato);

            //Valida llaves a restaurar segun el tipo de dato
            if(!string.IsNullOrEmpty(llavesRestaurar))
                ValidaFiltro(llavesRestaurar, tipoDato);

            //Valida ruta del archivo
            if (string.IsNullOrEmpty(rutaArchivo))
            {
                throw new ApplicationException(ObtenerDescripcionEnum(mensajes.RutaVacia));
            }

            //Valida nombre del archivo
            if (string.IsNullOrEmpty(nombreArchivo))
            {
                throw new ApplicationException(ObtenerDescripcionEnum(mensajes.NombreArchivoVacio));
            }

            //Desempaca archivos contenidos en zip
            LeerArchivoZip(rutaArchivo, nombreArchivo);

            //Leer archivos en directorio
            var lee = new DirectoryInfo(rutaArchivo);
            foreach (var archivo in lee.GetFiles().Where(o => o.Extension != ".zip"))
            {                
                //Genera archivo csv
                var dt = ObtenerDatosCsv(Path.Combine(rutaArchivo, archivo.Name), tipoDato, llavesRestaurar);
                //Sube registros de archivo csv
                RestaurarTablaTemporal(dt, campo, tipoDato, llavesRestaurar, tablaRestaurar, sobreescribirBaseDatos);

                //Se elimina archivo csv
                if (File.Exists(Path.Combine(rutaArchivo, archivo.Name)))
                    File.Delete(Path.Combine(rutaArchivo, archivo.Name));
            }
        }

        /// <summary>
        /// Función para validar tipo de dato entero
        /// </summary>
        /// <returns></returns>
        public static bool ValidaTipoEntero(string llaves)
        {
            if (llaves.Split(',').Any(digito => digito.Any(numero => !char.IsDigit(numero))))
                throw new InvalidDataException("No es un valor númerico");

            return true;
        }

        /// <summary>
        /// Función para validar tipo de dato decimal
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static bool ValidaTipoDecimal(string llaves)
        {

            if (llaves.Split(',').Any(digito => digito.Any(numero => !char.IsDigit(numero) && numero.ToString() != ".")))
            {
                throw new InvalidDataException("No es un valor númerico decimal");
            }

            if (llaves.Split(',').Any(numero => numero.Count(f => f == '.') > 1))
            {
                throw new InvalidDataException("Tiene más de un punto decimal");
            }

            return true;
        }

        /// <summary>
        /// Función para validar tipo de dato fecha
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static bool ValidaTipoFecha(string llaves)
        {
            // +++
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                //valida tamaño data1
                if (data1.Length != 8 || data2.Length != 8)
                {
                    throw new InvalidDataException("Fecha No valida, tamaño de la fecha deber ser de 8 caracteres");
                }

                //Valida que fecha contenga solo digitos
                if (data1.Any(numero => !char.IsNumber(numero)) || data2.Any(numero => !char.IsNumber(numero)))
                {
                    throw new InvalidDataException("Fecha No valida, solo debe contener dígitos");
                }

                //valida mes
                if (!ValidaMes(data1.Substring(4, 2)) || !ValidaMes(data2.Substring(4, 2)))
                {
                    throw new InvalidDataException("Fecha No valida, mes no valido");
                }

                //valida dia
                if (!ValidaDia(data1) || !ValidaDia(data2))
                    throw new InvalidDataException("Fecha No valida, día no valido");
            }
            // +++
            return true;
        }

        /// <summary>
        /// Función para validar tipo de dato alfanúmerico
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static bool ValidaTipoTexto(string llaves)
        {
            //Se valida que los datos dentro de la cadena sean de tipo alfanúmericos
            if (llaves.Split(',').Any(digito => digito.Any(numero => !char.IsLetterOrDigit(numero))))
                throw new InvalidDataException("No es un valor alfanúmerico");

            return true;
        }

        /// <summary>
        /// Función para generar un data table con información de un csv, registros con información extraida de una tabla de una base de datos
        /// </summary>
        /// <param name="rutaNombreArchivo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <returns></returns>
        private static DataTable ObtenerDatosCsv(string rutaNombreArchivo, string tipoDato, string llavesRestaurar)
        {
            var datosCsv = new DataTable();

            using (var csvReader = new TextFieldParser(rutaNombreArchivo))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;
                var columnas = csvReader.ReadFields();

                //Se agregan las columnas del DataTable (Encabezados)
                if (columnas != null)
                {
                    foreach (var columna in columnas)
                    {
                        var nombreColumnas = new DataColumn(columna) { AllowDBNull = true };
                        datosCsv.Columns.Add(nombreColumnas);
                    }
                }

                //Lee los datos
                while (!csvReader.EndOfData)
                {
                    var datosColumnas = csvReader.ReadFields();

                    if (datosColumnas == null) continue;
                    //Si dato es vacío se asigna nulo
                    for (var i = 0; i < datosColumnas.Length; i++)
                    {
                        if (datosColumnas[i] == "")
                            datosColumnas[i] = null;
                    }

                    datosCsv.Rows.Add(datosColumnas);
                }
            }

            return datosCsv;
        }

        private static readonly Regex SinEspacios = new Regex(@"\s+");

        /// <summary>
        /// Elimina espacios de un string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string EliminaEspacios(string input)
        {
            return SinEspacios.Replace(input, "");
        }

        /// <summary>
        /// Función para subir datos a una base de datos, se suben primeramente a una tabla temporal y posteriormente se filtra
        /// la información para luego ser ingresada a la base de datos.
        /// </summary>
        /// <param name="csvFileData"></param>
        /// <param name="campo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <param name="tablaRestaurar"></param>
        public static void RestaurarTablaTemporal(DataTable csvFileData, string campo, string tipoDato, string llavesRestaurar, string tablaRestaurar, bool sobreescribirBaseDatos)
        {
            try
            {
                using (var conexion = new SqlConnection(ObtieneStringConexion()))
                {
                    var comando = conexion.CreateCommand();

                    //Se obtiene información de la tabla
                    comando.CommandText = "select * from information_schema.columns where table_name = @tableName";
                    comando.Parameters.Add("@tableName", SqlDbType.VarChar).Value = tablaRestaurar;
                    comando.CommandType = CommandType.Text;

                    conexion.Open();
                    var columnList = new List<Columnas>();

                    using (IDataReader reader = comando.ExecuteReader(CommandBehavior.KeyInfo))
                    {
                        while (reader.Read())
                        {
                            //Obtiene información de cada columna de la tabla.
                            columnList.Add(new Columnas().ReadFromReader(reader));
                        }
                    }

                    var temporal = "create table {0} ({1})";
                    var sb = new StringBuilder();

                    foreach (var column in columnList)
                    {
                        // Ciclo para contruir create de tabla temporal.
                        sb.Append(column.ToString());
                    }

                    //Se crea la tabla temporal con la información de la tabla física a restaurar
                    var create = string.Format(temporal, "#TempTable", string.Join(",", columnList.Select(c => c.ToString()).ToArray()));
                    var DbCommand = new SqlCommand(create, conexion);
                    DbCommand.ExecuteNonQuery();

                    //Llena tabla temporal con datos del csv
                    using (var bulkCopy = new SqlBulkCopy(conexion))
                    {
                        bulkCopy.DestinationTableName = "#TempTable";
                        bulkCopy.WriteToServer(csvFileData);
                    }

                    //Where con los filtros o llaves a restaurar
                    var where = string.Empty;
                    if(!string.IsNullOrEmpty(llavesRestaurar))
                        where = GeneraWhere(campo, tipoDato, llavesRestaurar, true);

                    var archivosRestaurar = new DataTable();

                    if (sobreescribirBaseDatos)
                    {
                        //Select de la tabla temporal con los filtros o llaves a restaurar
                        DbCommand.CommandText = "SELECT * FROM dbo.#TempTable " + where;
                        var dr = DbCommand.ExecuteReader();
                        archivosRestaurar.Load(dr);
                        dr.Close();

                        //Se eliminan todos los registros
                        EliminaDatos("DELETE from " + tablaRestaurar + where);
                    }
                    else
                    {
                        //Filtro con ligas entre llave primaria
                        var liga = LlavePrimaria(tablaRestaurar, false).Split(',').FirstOrDefault();
                        //Campo a validar si es nulo
                        var nulo = LlavePrimaria(tablaRestaurar, !string.IsNullOrEmpty(where)).Split(',').LastOrDefault();

                        //Join de la tabla temporal y la tabla física a Restaurar
                        DbCommand.CommandText = "SELECT a.* FROM dbo.#TempTable a LEFT JOIN " + tablaRestaurar
                                                + liga
                                                + where
                                                + nulo;
                        var dr = DbCommand.ExecuteReader();
                        archivosRestaurar.Load(dr);
                        dr.Close();
                    }

                    //Restaurar los datos de la tabla temporal a la tabla fisica
                    if (archivosRestaurar.Rows.Count > 0)
                        RestaurarDatosTabla(archivosRestaurar, tablaRestaurar);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error al Restaurar Tabla Temporal. {0}", ex);
            }

        }

        /// <summary>
        /// Función para obtener la llave primaria de una tabla, y generar los filtros necesarios para obtener
        /// sólo los registros que se van a restaurar
        /// </summary>
        /// <param name="tabla"></param>
        /// <param name="condicion"></param>
        /// <returns></returns>
        public static string LlavePrimaria(string tabla, bool condicion)
        {
            var filtro = string.Empty;
            var nulo = string.Empty;

            var consulta =
                "SELECT ColumnName = col.column_name FROM information_schema.table_constraints tc " +
                "INNER JOIN information_schema.key_column_usage col " +
                "ON col.Constraint_Name = tc.Constraint_Name " +
                "AND col.Constraint_schema = tc.Constraint_schema " +
                "WHERE tc.Constraint_Type = 'Primary Key' AND col.Table_name = '" + tabla + "'" +
                " order by ordinal_position";

            var ds = Consulta(consulta);
            if (ds.Tables[0].Rows.Count <= 0)
                throw new Exception("Tabla no tiene llave primaria !!!");
            else
            {
                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {                            
                        nulo =  (condicion ? " AND": " WHERE") + " b." + ds.Tables[0].Rows[i][0] + " is null";

                        filtro = " b ON a." + ds.Tables[0].Rows[i][0] + " = b." + ds.Tables[0].Rows[i][0];
                    }
                    else
                        filtro = filtro + " AND a." + ds.Tables[0].Rows[i][0] + " = b." + ds.Tables[0].Rows[i][0];
                }
            }

            return filtro + "," + nulo;
        }

        /// <summary>
        /// Función útil para obtener orden (order by) con llave primaria de una tabla de una base de datos sql
        /// </summary>
        /// <param name="tabla"></param>
        /// <param name="orden"></param>
        /// <returns></returns>
        public static string LlavePrimariaOrden(string tabla, string orden, bool alias)
        {
            var llavePrimaria = string.Empty;

            //Se construye string con la consulta para obtener order by con la llave primaria de una tabla
            var consulta =
                "SELECT ColumnName = col.column_name FROM information_schema.table_constraints tc " +
                "INNER JOIN information_schema.key_column_usage col " +
                "ON col.Constraint_Name = tc.Constraint_Name " +
                "AND col.Constraint_schema = tc.Constraint_schema " +
                "WHERE tc.Constraint_Type = 'Primary Key' AND col.Table_name = '" + tabla + "'" +
                "order by ordinal_position";

            //Ejecuta consulta
            var ds = Consulta(consulta);

            if (ds.Tables[0].Rows.Count <= 0)
                throw new Exception("Tabla no tiene llave primaria !!!");
            else
            {
                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    llavePrimaria = llavePrimaria + (alias ? "a." : "") + ds.Tables[0].Rows[i][0] + " " + orden + ",";
                }
            }

            return llavePrimaria.TrimEnd(',');
        }

        /// <summary>
        /// Clase con las propiedades de una columna de una tabla. Entre las propiedades tenemos: nombre, tipo de dato,
        /// longitud, y si acepta nulos.
        /// </summary>
        public class Columnas
        {
            public string Nombre { get; set; }
            public string TipoDato { get; set; }
            public int Indice { get; set; }
            public bool EsNulo { get; set; }
            public string LongitudMaxima { get; set; }

            protected string MaxLengthFormatted => LongitudMaxima.Equals("-1") ? "max" : LongitudMaxima;

            /// <summary>
            /// Función para obtener datos del reader y llenar las propiedades de una columna: nombre, tipo de dato, longitud y si acepta nulos
            /// </summary>
            /// <param name="reader"></param>
            /// <returns></returns>
            public Columnas ReadFromReader(IDataReader reader)
            {
                //Información de las columnas, nombre, tipo de datos, posición, es nulo y longitud en caso de ser tipo caracter
                this.Nombre = reader["COLUMN_NAME"].ToString();
                this.TipoDato = reader["DATA_TYPE"].ToString();
                this.Indice = (int)reader["ORDINAL_POSITION"];
                this.EsNulo = ((string)reader["IS_NULLABLE"]) == "YES";
                this.LongitudMaxima = reader["CHARACTER_MAXIMUM_LENGTH"].ToString();

                return this;
            }

            /// <summary>
            /// Función para regresar el formato de una columna con su nombre, tipo de dato, longitud y si acepta nulos.
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return string.Format("[{0}] {1}{2} {3}NULL", Nombre, TipoDato,
                    LongitudMaxima == string.Empty ? "" : "(" + MaxLengthFormatted + ")",
                    EsNulo ? "" : "NOT ");
            }

        }

        /// <summary>
        /// Restaurar datos a una tabla mediante de un archivo csv mediante un BulkCopy
        /// </summary>
        /// <param name="csvFileData"></param>
        /// <param name="tablaRestaurar"></param>
        public static void RestaurarDatosTabla(DataTable csvFileData, string tablaRestaurar)
        {
            //var conn = ObtenerConexion();            
            using (var volcado = new SqlBulkCopy(ObtieneStringConexion(), SqlBulkCopyOptions.KeepIdentity))
            {
                volcado.DestinationTableName = tablaRestaurar;
                foreach (var column in csvFileData.Columns)
                    volcado.ColumnMappings.Add(column.ToString(), column.ToString());
                volcado.WriteToServer(csvFileData);
            }
        }

        /// <summary>
        /// Función para leer un archivo dentro de un Zip
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <returns></returns>
        public static void LeerArchivoZip(string rutaArchivo, string nombreArchivo)
        {
            try
            {
                using (var zipFile = ZipFile.Read(Path.Combine(rutaArchivo, nombreArchivo)))
                {
                    foreach (var archivo in zipFile.Entries)
                    {
                        var ms = Desempacar(archivo);
                        var nombre = Path.GetFileName(archivo.FileName);
                        if (nombre != null)
                        {
                            var file = new FileStream(Path.Combine(rutaArchivo, nombre), FileMode.Create, FileAccess.Write);
                            ms.WriteTo(file);
                            file.Close();
                        }
                        ms.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error al leer archivo Zip. {0}", ex);
            }


        }

        /// <summary>
        /// Función para desempacar archivo de un zip
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private static MemoryStream Desempacar(ZipEntry entry)
        {
            var buffer = new byte[entry.UncompressedSize];
            var ms = new MemoryStream(buffer);
            entry.Extract(ms);
            return ms;
        }

        /// <summary>
        /// Función para respaldar una tabla para un campo y registros (llaves) elegidos. Se pueden
        /// eliminar los registros de la tabla y se genera un archivo con estructura csv.
        /// </summary>
        /// <param name="tablaRespaldar"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRespaldar"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="limpiarRegistros"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="guardarTitulos"></param>
        public void _respaldador(string tablaRespaldar, string campoFiltrado, string tipoDato, string llavesRespaldar,
            string rutaArchivo, string nombreArchivo, bool limpiarRegistros, bool sobreescribirarchivo, bool guardarTitulos)
        {
            //Se valida si existe tabla a respaldar
            if (!TablaExiste(tablaRespaldar)) return;

            //Se valida si existe campo
            if (!CampoExiste(tablaRespaldar, campoFiltrado)) return;

            //Se valida el tipo de dato
            ValidaTipoDato(tipoDato);

            //Se obtienen columnas de la tabla a respaldar
            var columnas = ObtieneColumnas(ObtieneId(tablaRespaldar));

            //Si hay llaves a respaldar
            if (string.IsNullOrEmpty(llavesRespaldar))
            {
                throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlavesRespaldoVacias));
            }

            //Valida Filtro
            ValidaFiltro(llavesRespaldar, tipoDato);

            //Se obtiene filtro para las llaves a respaldar capturadas
            var where = GeneraWhere(campoFiltrado, tipoDato, llavesRespaldar, false);

            //Si el filtro se genero correctamente se crean archivos csv y archivo zip
            if (!string.IsNullOrEmpty(where))
            {
                var conteo = 0;
                var i = 0;
                conteo = (int)Consulta("Select Count(*) from " + tablaRespaldar + where).Tables[0].Rows[0][0];

                //Valida que las llaves arrojen resultado
                if (conteo == 0)
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.ConsultaSinResultados));

                do
                {
                    var take = conteo > 500000 ? 500000 : conteo;

                    //La primer consulta es de orden ascendente
                    var orden = i == 0 ? "ASC" : "DESC";

                    //Se forma string con la consulta a ejecutar
                    var consulta = "WITH CTE AS(SELECT TOP " + take + " " + columnas + " FROM " + tablaRespaldar + where + " ORDER BY " 
                        + LlavePrimariaOrden(tablaRespaldar, orden, false) + ")"
                        + "SELECT * FROM CTE ORDER BY " + LlavePrimariaOrden(tablaRespaldar, "", false);

                    //Se ejecuta consulta
                    var datosCsv = Consulta(consulta).Tables[0];

                    //Se genera archivo csv para la consulta ejecutada en el paso anterior
                    GeneraCsv(rutaArchivo, nombreArchivo, sobreescribirarchivo, guardarTitulos, datosCsv, i);

                    //Cantidad restante del conteo
                    conteo = conteo - take;
                    i++;

                    //Eliminar registros de la tabla de la base de datos
                    if (limpiarRegistros)
                    {
                        EliminaDatos("DELETE TOP (" + take + ") from " + tablaRespaldar + where);
                    }

                } while (conteo > 0);

                //Se genera archivo zip con los csv generados
                GenerarArchivoZip(rutaArchivo, sobreescribirarchivo, nombreArchivo);
            }
        }

        /// <summary>
        /// Función que ejecuta una consulta en SQL
        /// </summary>
        /// <param name="consulta"></param>
        /// <returns></returns>
        public static DataSet Consulta(string consulta)
        {
            var ds = new DataSet();
            var dt = new DataTable();
            var conn = ObtenerConexion();
            var comando = new SqlCommand(consulta, conn);
            dt.Load(comando.ExecuteReader());
            ds.Tables.Add(dt);
            conn.Close();

            return ds;
        }

        /// <summary>
        /// Función para eliminar registros de una tabla
        /// </summary>
        /// <param name="elimina"></param>
        public static void EliminaDatos(string elimina)
        {
            var conn = ObtenerConexion();
            var comandoElimina = new SqlCommand(elimina, conn);
            comandoElimina.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Función para validar los tipos de datos númericos, string y fecha
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static void ValidaTipoDato(string tipo)
        {
            /* Tipos de Datos */
            switch (tipo.ToLower())
            {
                /* Numericos Exactos sin Decimales */
                case "bigint":
                case "bit":
                case "int":
                case "smallint":
                case "tinyint":
                    break;
                /* Numericos Exactos con Decimales */
                case "decimal":
                case "money":
                case "numeric":
                case "smallmoney":
                /* Numericos Aproximados */
                case "float":
                case "real":
                    break;
                /* Fecha y Hora*/
                case "date":
                case "smalldatetime":
                case "datetime":
                    break;
                /* Strings */
                case "char":
                case "varchar":
                case "text":
                    break;
                default:
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.TipoDatoNoValido));
            }
        }

        /// <summary>
        /// Función para validar los tipos de datos númericos, string y fecha
        /// </summary>
        /// <param name="llaves"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static void ValidaFiltro(string llaves, string tipo)
        {
            /* Tipos de Datos */
            switch (tipo.ToLower())
            {
                /* Numericos Exactos sin Decimales */
                case "bigint":
                case "bit":
                case "int":
                case "smallint":
                case "tinyint":
                    ValidaDatosNumericosSinDecimales(llaves);
                    break;
                /* Numericos Exactos con Decimales */
                case "decimal":
                case "money":
                case "numeric":
                case "smallmoney":
                /* Numericos Aproximados */
                case "float":
                case "real":
                    ValidaDatosNumericosConDecimales(llaves);
                    break;
                /* Fecha y Hora*/
                case "date":
                case "smalldatetime":
                case "datetime":
                    ValidaDatosFecha(llaves);
                    break;
                /* Strings */
                case "char":
                case "varchar":
                case "text":
                    ValidaDatosString(llaves);
                    break;
                default:
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.TipoDatoNoValido));
            }
        }

        /// <summary>
        /// Función para obtener el filtro de cualquier tipo de dato, ya 
        /// sea númerico, fecha o de tipo string. La función genera una
        /// cadena o string con el filtro generado tomando en cuenta el 
        /// tipo de dato y las llaves o registros a filtrar.
        /// </summary>
        /// <param name="campoFiltrado"></param>
        /// <param name="tipo"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string GeneraWhere(string campoFiltrado, string tipo, string llaves, bool alias)
        {
            string filtro;

            /* Tipo de Datos */
            switch (tipo.ToLower())
            {
                /* Numericos Exactos sin Decimales */
                case "bigint":
                case "bit":
                case "int":
                case "smallint":
                case "tinyint":
                /* Numericos Exactos con Decimales */
                case "decimal":
                case "money":
                case "numeric":
                case "smallmoney":
                /*** Numericos Aproximados ***/
                case "float":
                case "real":
                    filtro = ObtenerFiltro(tipo, campoFiltrado, llaves, alias);
                    break;
                /*** Fecha y Hora ***/
                case "date":
                case "smalldatetime":
                case "datetime":
                    filtro = ObtenerFiltroDatosFecha(tipo, campoFiltrado, llaves, alias);
                    break;
                /* Strings */
                case "char":
                case "varchar":
                case "text":
                    filtro = ObtenerFiltro(tipo, campoFiltrado, llaves, alias);
                    break;
                default:
                    // Se pasa vacío ya que la función ValidaFiltro hace la tarea de 
                    // escribir el mensaje de error, por tanto no es necesario, un manejo
                    // en esta parte, pero se agrega el default por consistencia.
                    filtro = string.Empty;
                    break;
            }

            return filtro;
        }

        /// <summary>
        /// Función para validar si el tipo es texto lo regresa entre comillas.
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string TextoConComillas(string tipo, string data)
        {
            string concomillas;

            switch (tipo.ToLower())
            {
                case "char":
                case "varchar":
                case "text":
                    concomillas = "'" + data + "'";
                    break;
                default:
                    concomillas = data;
                    break;
            }

            return concomillas;
        }

        /// <summary>
        /// Función para validar datos tipo texto
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static void ValidaDatosString(string llaves)
        {
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Any(numero => !char.IsLetterOrDigit(numero)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }

                if (data2.Any(numero => !char.IsLetterOrDigit(numero)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }
            }
        }

        /// <summary>
        /// Función para validar que los datos númericos a filtrar son 
        /// de tipo númerico sin decimales, se pueden manejar rangos 
        /// separados por el símbolo - y se pueden separar elementos mediante
        /// comas, solo se permite capturar elementos de tipo númerico.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static void ValidaDatosNumericosSinDecimales(string llaves)
        {
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Any(numero => !char.IsDigit(numero)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }

                if (data2.Any(numero => !char.IsDigit(numero)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }
            }
        }

        /// <summary>
        /// Función para validar que los datos o llaves a respaldar correspondan
        /// a los datos númericos con decimales, permite manejo de rangos separados 
        /// por el símbolo - y separar elementos por comas, y cada elemento permite 
        /// sólo un punto decimal y sólo se deben capturar números. Cualquier error
        /// se escribe sobre el archivo de salida el tipo de error que se genero.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static void ValidaDatosNumericosConDecimales(string llaves)
        {
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Count(f => f == '.') > 1)
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }

                if (data1.Any(numero => !char.IsNumber(numero) && numero.ToString() != "."))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }

                if (data2.Count(f => f == '.') > 1)
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }
                if (data2.Any(numero => !char.IsDigit(numero) && numero.ToString() != "."))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.LlaveNoValida));
                }
            }
        }

        /// <summary>
        /// Función para validar que los datos o llaves a respaldar correspondan
        /// al tipo de datos fecha, maneja rangos separados por el símbolo - y comas 
        /// para separar las fechas, además solo permite números. 
        /// Ejemplo: 20170101-20170131,20170201 
        /// Los errores se escriben en el archivo de salida.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static void ValidaDatosFecha(string llaves)
        {
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                //valida tamaño data1
                if (data1.Length != 8 || data2.Length != 8)
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.FechaNoValida));
                }

                //Valida que fecha contenga solo digitos
                if (data1.Any(numero => !char.IsNumber(numero)) || data2.Any(numero => !char.IsNumber(numero)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.FechaNoValida));
                }

                //valida mes
                if (!ValidaMes(data1.Substring(4, 2)) || !ValidaMes(data2.Substring(4, 2)))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.MesNoValido));
                }

                //valida dia
                if (!ValidaDia(data1) || !ValidaDia(data2))
                {
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.DiaNoValido));
                }
            }
        }

        /// <summary>
        /// Función para valdiar el mes, entre 1 y 12.
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        public static bool ValidaMes(string mes)
        {
            var valido = (int.Parse(mes) > 1 && int.Parse(mes) < 12);
            return valido;
        }

        /// <summary>
        /// Función para validar el día, toma como entrada la fecha en formato universal yyyyMMdd.
        /// Verifica si el año es bisiesto, y si el número de día es valido para el mes, ya sea
        /// 30, 31, 28 o 29 según el mes.
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static bool ValidaDia(string fecha)
        {
            var valido = false;
            var year = fecha.Substring(0, 4);
            var mes = fecha.Substring(4, 2);
            var dia = fecha.Substring(6, 2);
            var bisiesto = DateTime.IsLeapYear(int.Parse(year)) ? 29 : 28;

            switch (int.Parse(mes))
            {
                //31 Días
                case 1: //Enero
                case 3: //Marzo                
                case 5: //Mayo                
                case 7: //Julio
                case 8: //Agosto
                case 10: //Octubre
                case 12: //Diciembre
                    if (int.Parse(dia) >= 1 || int.Parse(dia) <= 31)
                        valido = true;
                    break;
                case 4: //Abril
                case 6: //Junio
                case 9: //Septiembe
                case 11: //Noviembre
                    if (int.Parse(dia) >= 1 || int.Parse(dia) <= 30)
                        valido = true;
                    break;
                case 2: //Febrero
                    if ((int.Parse(dia) >= 1 || int.Parse(dia) <= bisiesto))
                        valido = true;
                    break;
                default:
                    throw new ApplicationException(ObtenerDescripcionEnum(mensajes.DiaNoValido));
            }

            return valido;
        }

        /// <summary>
        /// Función para generar el filtro.
        /// </summary>
        /// <param name="tipoDato"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string ObtenerFiltro(string tipoDato, string campoFiltrado, string llaves, bool alias)
        {
            var filtro = string.Empty;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                var data2 = item.Split('-').LastOrDefault();

                if (string.IsNullOrEmpty(filtro))
                {

                    if (alias)
                        campoFiltrado = "a." + campoFiltrado;

                    if (data1 == data2)
                        filtro = " WHERE " + campoFiltrado + " = " + TextoConComillas(tipoDato, data1);
                    else
                        filtro = " WHERE " + campoFiltrado + " BETWEEN " + TextoConComillas(tipoDato, data1)
                            + " AND " + TextoConComillas(tipoDato, data2);
                }
                else
                {
                    if (data1 == data2)
                    {
                        filtro = filtro + " OR " + campoFiltrado + " = " + TextoConComillas(tipoDato, data1);
                    }
                    else
                        filtro = filtro + " OR " + campoFiltrado + " BETWEEN " + TextoConComillas(tipoDato, data1)
                            + " AND " + TextoConComillas(tipoDato, data2);
                }
            }

            return filtro;
        }

        /// <summary>
        /// Función para generar el filtro para los tipos de datos fecha. Smalldatetime y datetime
        /// se convierte a Date, ya que solo se filtra por fecha.
        /// </summary>
        /// <param name="tipoDato"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string ObtenerFiltroDatosFecha(string tipoDato, string campoFiltrado, string llaves, bool alias)
        {
            var filtro = string.Empty;
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                var data2 = item.Split('-').LastOrDefault();

                if (string.IsNullOrEmpty(filtro))
                {

                    if (alias)
                        campoFiltrado = "a." + campoFiltrado;

                    if (data1 == data2)
                        filtro = " WHERE CONVERT(DATE, " + campoFiltrado + ") = CONVERT(DATE, '" + data1 + "')";
                    else
                        filtro = " WHERE CONVERT(DATE, " + campoFiltrado + ") BETWEEN CONVERT(DATE, '" + data1 + "')"
                            + " AND CONVERT(" + tipoDato + ", '" + data2 + "')";
                }
                else
                {
                    if (data1 == data2)
                        filtro = filtro + " OR " + campoFiltrado + " = CONVERT(DATE, '" + data1 + "')";
                    else
                        filtro = filtro + " OR " + campoFiltrado + " BETWEEN CONVERT(DATE, '" + data1 + "')"
                            + " AND CONVERT(DATE, '" + data2 + "')";
                }
            }

            return filtro;
        }

        /// <summary>
        /// Función para generar archivo CSV, encabezados y registros, así como guardar el archivo
        /// en una ruta dada. Los encabezados (títulos) y sobreescritura del archivo son parámetros
        /// que pueden o no incluir los títulos o sobreescritura del archivo, si no se permite la
        /// sobreescritura genera una versión del archivo con un nombre sugerido.
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="guardarTitulos"></param>
        /// <param name="dt"></param>
        /// <param name="nombreSec"></param>
        public static void GeneraCsv(string rutaArchivo, string nombreArchivo, bool sobreescribirarchivo, bool guardarTitulos, DataTable dt, int nombreSec)
        {
            //Nombre del archivo csv con un numero consecutivo
            var nombreNuevo = Path.GetFileNameWithoutExtension(nombreArchivo) + "-" + nombreSec + Path.GetExtension(nombreArchivo);
            //Archivo csv debido al tamaño que puede alcanzar, se deben generar los archivos físicamente
            var archivo = new StreamWriter(Path.Combine(rutaArchivo, nombreNuevo));

            //Encabezados o columnas del archivo csv
            if (guardarTitulos)
            {
                for (var i = 0; i < dt.Columns.Count; i++)
                {
                    archivo.Write(dt.Columns[i]);
                    if (i < dt.Columns.Count - 1)
                    {
                        //Nombre de las columnas separados por comas
                        archivo.Write(",");
                    }
                }
                //Escribe encabezados en archivo csv
                archivo.Write(archivo.NewLine);
            }

            //Datos o registros del archivo csv
            foreach (DataRow dr in dt.Rows)
            {
                for (var i = 0; i < dt.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        var value = dr[i].ToString();
                        if (value.Contains(','))
                        {
                            value = $"\"{value}\"";
                            archivo.Write(value);
                        }
                        else
                        {
                            archivo.Write(dr[i].ToString());
                        }
                    }
                    if (i < dt.Columns.Count - 1)
                    {
                        //Reistro con los Valores de las columnas separados por comas, si existen mas datos escribe una coma
                        archivo.Write(",");
                    }
                }
                archivo.Write(archivo.NewLine);
            }

            //Se escribe archivo csv
            archivo.Close();
        }

        /// <summary>
        /// Función para generar archivo zip y genera el archivo. Si el parámetro sobreescribirarchivo
        /// esta con valor falso, no se genera el archivo y se genera un mensaje.
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="nombreArchivo"></param>
        public void GenerarArchivoZip(string rutaArchivo, bool sobreescribirarchivo, string nombreArchivo)
        {
            //Si no se sobreescribe y archivo existe
            var nombreZip = Path.ChangeExtension(nombreArchivo, "zip");
            if (nombreZip != null)
            {
                var nombreZipRuta = Path.Combine(rutaArchivo, nombreZip);

                if (File.Exists(nombreZipRuta))
                {
                    if (!sobreescribirarchivo)
                    {
                        throw new ApplicationException(ObtenerDescripcionEnum(mensajes.ArchivoYaExiste));
                    }
                }
            }

            GeneraZip(rutaArchivo, nombreArchivo);
        }

        /// <summary>
        /// Función para genera archivo zip
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        public void GeneraZip(string rutaArchivo, string nombreArchivo)
        {
            try
            {
                //Si no existe la ruta, intenta crear el directorio
                if (rutaArchivo != null && !Directory.Exists(rutaArchivo))
                    Directory.CreateDirectory(rutaArchivo);

                // Generar el archivo en formato zip y dentro el archivo con el nombre de archivo capturado
                var zip = new ZipFile();
                if (rutaArchivo == null)
                {
                    throw new ApplicationException("Error al generar archivo Zip, rutaArchivo nula.");
                }

                //Se agregan archivos csv a archivo zip
                var agrega = new DirectoryInfo(rutaArchivo);
                foreach (var archivo in agrega.GetFiles().Where(o => o.Extension != ".zip"))
                {
                    //Se agrega archivo csv a archivo zip
                    zip.AddFile(Path.Combine(rutaArchivo, archivo.Name));
                }

                //Se genera archivo zip
                zip.Save(Path.ChangeExtension(Path.Combine(rutaArchivo, nombreArchivo), "zip"));

                //Se eliminan los archivos csv generados
                var elimina = new DirectoryInfo(rutaArchivo);
                foreach (var archivo in elimina.GetFiles().Where(o => o.Extension != ".zip"))
                {
                    //Se elimina archivo csv
                    if (File.Exists(Path.Combine(rutaArchivo, archivo.Name)))
                        File.Delete(Path.Combine(rutaArchivo, archivo.Name));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error al generar archivo Zip. {0}", ex);
            }
        }

        /// <summary>
        /// Función para generar archivo
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sb"></param>
        public void GeneraArchivo(string rutaArchivo, string nombreArchivo, StringBuilder sb)
        {
            try
            {
                //Si no existe la ruta, intenta crear el directorio
                if (rutaArchivo != null && !Directory.Exists(rutaArchivo))
                    Directory.CreateDirectory(rutaArchivo);

                File.WriteAllText(Path.Combine(rutaArchivo, nombreArchivo), sb.ToString());
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error al generar archivo. {0}", ex);
            }
        }

        /// <summary>
        /// Función para validar si existe una tabla
        /// </summary>
        /// <param name="nombreTabla"></param>
        /// <returns></returns>
        public static bool TablaExiste(string nombreTabla)
        {
            var ds = Consulta("select 1 from sys.objects where name = '" + nombreTabla + "'");

            if (ds.Tables[0].Rows.Count <= 0)
                throw new ApplicationException("Tabla no existe !!!");

            return true;
        }

        public static bool CampoExiste(string nombreTabla, string campo)
        {
            var ds = Consulta("select 1 FROM INFORMATION_SCHEMA.COLUMNS where column_name = '" + campo + "' and table_name = '" + nombreTabla + "'");

            if (ds.Tables[0].Rows.Count <= 0)
                throw new ApplicationException("Campo no existe !!!");

            return true;
        }

        /// <summary>
        /// Función para establecer una conexión a SQL
        /// </summary>
        /// <returns></returns>
        public static SqlConnection ObtenerConexion()
        {
            try
            {
                var conexion = new SqlConnection(ObtieneStringConexion());
                conexion.Open();
                return conexion;
            }
            catch (NullReferenceException)
            {
                throw new ApplicationException("Base de datos no definida");
            }
            catch (FormatException)
            {
                throw new ApplicationException("Error en la definición de la base de datos");
            }
            catch
            {
                throw new ApplicationException("No se pudo conectar a la base de datos.");
            }
        }

        /// <summary>
        /// Función para obtener id de una tabla, filtrando por el nombre de la tabla.
        /// </summary>
        /// <param name="nombreTabla"></param>
        /// <returns></returns>
        public int ObtieneId(string nombreTabla)
        {
            var id = 0;

            var ds = Consulta("select object_Id from sys.objects where name = '" + nombreTabla + "'");

            if (ds.Tables[0].Rows.Count > 0)
                id = int.Parse(ds.Tables[0].Rows[0][0].ToString());

            return id;
        }

        /// <summary>
        /// Función para obtener los nombres y id's separados por comas de las columnas de una
        /// tabla. Los nombres y id's se obtienen filtrando por el id (object_id) de una tabla.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string ObtieneColumnas(int id)
        {
            var columnas = string.Empty;
            var ds = Consulta("select name, column_id from sys.columns where object_id = '" + id + "'");
            if (ds.Tables[0].Rows.Count <= 0) return columnas.TrimEnd(',');
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                columnas = columnas + row[0] + ",";
            }
            return columnas.TrimEnd(',');
        }

        /// <summary>
        /// Función para obtener string de conexión en archivo de configuración
        /// </summary>
        /// <returns></returns>
        public static string ObtieneStringConexion()
        {
            return ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        }

        /// <summary>
        /// Función para obtener la descripción de un enum, útil para desplegar los mensajes de error,
        /// En el archivo de salida.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ObtenerDescripcionEnum(Enum value)
        {
            // Get the Description attribute value for the enum value
            var fi = value.GetType().GetField(value.ToString());
            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        /// <summary>
        /// Función para genera un timestamp, útil para nombrar los archivos cuyo nombre ya existe
        /// y no desea sobreecribir.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfff");
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}