﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="respalda.aspx.cs" Inherits="respaldador.respalda" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <title></title>
    <script>

        var inits = {
            datos: null
        }
        
        inits.datos = null;

        function obtieneDatos() {

            //Se valida que se elija la tabla a respaldar
            if ($("#tablaRespaldar").val() == 0) {
                alert("Seleccione la tabla a respaldar");
                return;
            }

            //Se agregan los parametros fuente y expediente en un arreglo
            var params = {
                tablaRespaldar: $("#tablaRespaldar").val(),
                campoFiltrado: $("#campoFiltrado").val(),
                tipoCampo: $("#tipoCampo").val(),
                llavesRespaldar: $("#llavesRespaldar").val(),
                rutaArchivo: $("#rutaArchivo").val(),
                nombreArchivo: $("#nombreArchivo").val(),
                limpiarRegistros: $("#limpiarRegistros").prop("checked") ? true : false,
                sobreescribeArchivo: $("#sobreescribeArchivo").prop("checked") ? true : false,
                guardarTituloCampos: $("#guardarTituloCampos").prop("checked") ? true : false
            }
           
            //Acceso a base de datos
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "obtenerDatos.ashx",
                data: params,
                success: function (d) {                    
                },
                error: function (err, x, m) {
                    debugger;
                }
            });            
        }

        //Se limpian campos de captura de tolerancias
        function limpiarTolerancias() {
            $("#divTolerancias input").val('');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div id="divTolerancias" class="row" style="padding-top: 10px;">
                <div class="col-md-12 text-left">
                    <div class="col-xs-12">
                    </div>
                    <div class="col-xs-12">
                        <label for="tablaRespaldar">Tabla a Respaldar</label>
                        <input class="form-control" id="tablaRespaldar" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="campoFiltrado">Campo de filtrado</label>
                        <input class="form-control" id="campoFiltrado" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="tipoCampo">Tipo de campo</label>
                        <input class="form-control" id="tipoCampo" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="llavesRespaldar">Llaves a respaldar</label>
                        <input class="form-control" id="llavesRespaldar" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="rutaArchivo">Ruta del archivo</label>
                        <input class="form-control" id="rutaArchivo" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="nombreArchivo">Nombre archivo</label>
                        <input class="form-control" id="nombreArchivo" type="text" />
                    </div>
                    <div class="col-xs-12">
                        <label for="limpiarRegistros">Limpiar registros respaldados</label>
                        <input type="checkbox" id="limpiarRegistros" data-toggle="toggle" />
                    </div>
                    <div class="col-xs-12">
                        <label for="sobreescribeArchivo">Sobreescribir el archivo</label>
                        <input type="checkbox" id="sobreescribeArchivo" data-toggle="toggle" />
                    </div>
                    <div class="col-xs-12">
                        <label for="guardarTituloCampos">Guardar titulo de campos</label>
                        <input type="checkbox" checked="checked" id="guardarTituloCampos" data-toggle="toggle" />
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 10px">
                <div class="col-md-6">
                    <button id="btnRespaldar" type="button" class="btn btn-info" onclick="obtieneDatos()">Respaldar</button>
                </div>
                <div class="col-md-6 text-right">
                    <button id="btnLimpiar" type="button" class="btn" onclick="limpiar()">Limpiar Campos</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
