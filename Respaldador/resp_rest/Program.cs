﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ionic.Zip;
using respaldador;
using Microsoft.VisualBasic.FileIO;

namespace resp_rest
{
    class Program
    {
        static void Main(string[] args)
        {
            var tablaRespaldar = "";
            var campoFiltrado = "";
            var tipoCampo = "";
            var llavesRespaldar = "";
            var rutaArchivo = "";
            var nombreArchivo = "";
            var limpiarRegistros = false;
            var sobreescribirarchivo = false;
            var guardarTitulos = false;

            _respaldador(tablaRespaldar, campoFiltrado, tipoCampo, llavesRespaldar, rutaArchivo, nombreArchivo, limpiarRegistros, sobreescribirarchivo, guardarTitulos);

            var ruta = @"C:\cienciamed\csv\";
            var nombre = @"archivo00.zip";

            _restaurador("cienciamedCSV", "id", "int", "1-382280", ruta, nombre, false, ",", true);
        }

        /// <summary>
        /// Función para restaurar hacia una tabla temporal y filtrar registros contenidos en un csv para posteriormente generar 
        /// los datos en una tabla física.
        /// Si el párametro sql, es false entonces se filtra directamente desde el archivo csv para luego ingresar los datos en
        /// la tabla física.
        /// </summary>
        /// <param name="tablaRestaurar"></param>
        /// <param name="campo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sobreescribirBaseDatos"></param>
        /// <param name="separador"></param>
        /// <param name="sql"></param>
        public static void _restaurador(string tablaRestaurar, string campo, string tipoDato, string llavesRestaurar, string rutaArchivo, string nombreArchivo,
            bool sobreescribirBaseDatos, string separador, bool sql)
        {

            if (!TablaExiste(tablaRestaurar)) return;
            var s = LeerArchivoZip(Path.Combine(rutaArchivo, nombreArchivo));
            File.WriteAllText(Path.Combine(rutaArchivo, Path.ChangeExtension(nombreArchivo, "csv")), s);            
            var dt = (DataTable)ObtenerDatosCsv(Path.Combine(rutaArchivo, Path.ChangeExtension(nombreArchivo, "csv")), campo, tipoDato, llavesRestaurar, sql);            
            RestaurarTablaTemporal(dt, campo, tipoDato, llavesRestaurar, tablaRestaurar, sobreescribirBaseDatos);            
        }

        /// <summary>
        /// Función para leer un archivo dentro de un Zip
        /// </summary>
        /// <param name="ruta"></param>
        /// <returns></returns>
        public static string LeerArchivoZip(string ruta)
        {
            using (var zipFile = ZipFile.Read(ruta))
            {
                var entry = zipFile.Entries.FirstOrDefault();
                var ms = Desempacar(entry);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Función para desempacar archivo de un zip
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private static MemoryStream Desempacar(ZipEntry entry)
        {
            var buffer = new byte[entry.UncompressedSize];
            var ms = new MemoryStream(buffer);
            entry.Extract(ms);
            return ms;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutaNombreArchivo"></param>
        /// <param name="campo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        private static object ObtenerDatosCsv(string rutaNombreArchivo, string campo, string tipoDato, string llavesRestaurar, bool sql)
        {
            var datosCSV = new DataTable();

            try
            {
                //Se eliminan los espacios en blanco

                var llaves = EliminaEspacios(llavesRestaurar);

                var mensaje = _validaFiltro(llavesRestaurar, tipoDato);
                if (mensaje == 0)
                {
                    using (var csvReader = new TextFieldParser(rutaNombreArchivo))
                    {
                        csvReader.SetDelimiters(new string[] { "," });
                        csvReader.HasFieldsEnclosedInQuotes = true;
                        var columnas = csvReader.ReadFields();

                        //Indice de la columna igual al campo
                        var j = 0;
                        var indice = 0;//Indice de la columna a filtrar

                        //Se agregan las columnas del DataTable (Encabezados)
                        if (columnas != null)
                        {
                            foreach (var columna in columnas)
                            {
                                if (columna == campo)
                                    indice = j;

                                var nombreColumnas = new DataColumn(columna) { AllowDBNull = true };
                                datosCSV.Columns.Add(nombreColumnas);

                                j++;
                            }
                        }

                        //Lee los datos
                        while (!csvReader.EndOfData)
                        {
                            var datosColumnas = csvReader.ReadFields();

                            if (datosColumnas == null) continue;
                            //Si dato es vacío se asigna nulo
                            for (var i = 0; i < datosColumnas.Length; i++)
                            {
                                if (datosColumnas[i] == "")
                                    datosColumnas[i] = null;
                            }

                            if (sql)
                            {
                                //Se vacian todos los registros en una tabla temporal, ya que el filtro se realiza en Sql
                                datosCSV.Rows.Add(datosColumnas);
                            }
                            else
                            {
                                //Filtar segun las llaves capturadas
                                var data = datosColumnas[indice];
                                if (llaves.Contains(data))
                                    datosCSV.Rows.Add(datosColumnas);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Result = "ERROR",
                    ex.Message
                };
            }

            return datosCSV;
        }

        /// <summary>
        /// Elimina espacios de un string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string EliminaEspacios(string input)
        {
            return SinEspacios.Replace(input, "");
        }

        private static readonly Regex SinEspacios = new Regex(@"\s+");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFileData"></param>
        /// <param name="campo"></param>
        /// <param name="tipoDato"></param>
        /// <param name="llavesRestaurar"></param>
        /// <param name="tablaRestaurar"></param>
        public static object RestaurarTablaTemporal(DataTable csvFileData, string campo, string tipoDato, string llavesRestaurar, string tablaRestaurar, bool sobreescribirBaseDatos)
        {
            try
            {
                using (var conexion = new SqlConnection(ObtieneStringConexion()))
                {
                    var comando = conexion.CreateCommand();

                    //Se obtiene información de la tabla
                    comando.CommandText = "select * from information_schema.columns where table_name = @tableName";
                    comando.Parameters.Add("@tableName", SqlDbType.VarChar).Value = tablaRestaurar;
                    comando.CommandType = CommandType.Text;

                    conexion.Open();
                    var columnList = new List<Columnas>();

                    using (IDataReader reader = comando.ExecuteReader(CommandBehavior.KeyInfo))
                    {
                        while (reader.Read())
                        {
                            //Obtiene información de cada columna de la tabla.
                            columnList.Add(new Columnas().ReadFromReader(reader));
                        }
                    }

                    var temporal = "create table {0} ({1})";
                    var sb = new StringBuilder();

                    foreach (var column in columnList)
                    {
                        // Ciclo para contruir create de tabla temporal.
                        sb.Append(column.ToString());
                    }

                    //Se crea la tabla temporal con la información de la tabla física a restaurar
                    var create = string.Format(temporal, "#TempTable", string.Join(",", columnList.Select(c => c.ToString()).ToArray()));
                    var DbCommand = new SqlCommand(create, conexion);
                    DbCommand.ExecuteNonQuery();

                    //Llena tabla temporal con datos del csv
                    using (var bulkCopy = new SqlBulkCopy(conexion))
                    {
                        bulkCopy.DestinationTableName = "#TempTable";
                        bulkCopy.WriteToServer(csvFileData);
                    }

                    //Where con los filtros o llaves a restaurar
                    var where = GeneraWhere(campo, tipoDato, llavesRestaurar, true);

                    var archivosRestaurar = new DataTable();

                    if (sobreescribirBaseDatos)
                    {

                        //Select de la tabla temporal con los filtros o llaves a restaurar
                        DbCommand.CommandText = "SELECT * FROM dbo.#TempTable " + where;
                        var dr = DbCommand.ExecuteReader();
                        archivosRestaurar.Load(dr);
                        dr.Close();

                        //Se eliminan todos los registros
                        EliminaDatos("DELETE from " + tablaRestaurar + where);
                    }
                    else
                    {
                        //Filtro con ligas entre llave primaria
                        var liga = LlavePrimaria(tablaRestaurar).Split(',').FirstOrDefault();
                        //Campo a validar si es nulo
                        var nulo = LlavePrimaria(tablaRestaurar).Split(',').LastOrDefault();

                        //Join de la tabla temporal y la tabla física a Restaurar

                        DbCommand.CommandText = "SELECT a.* FROM dbo.#TempTable a LEFT JOIN " + tablaRestaurar + liga + where + nulo;
                        var dr = DbCommand.ExecuteReader();
                        archivosRestaurar.Load(dr);
                        dr.Close();
                    }

                    //Restaurar los datos de la tabla temporal a la tabla fisica
                    if (archivosRestaurar.Rows.Count > 0)
                        RestaurarDatosTabla(archivosRestaurar, tablaRestaurar);

                    return true;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Result = "ERROR",
                    ex.Message
                };
            }
        }

        /// <summary>
        /// Clase con las propiedades de una columna de una tabla. Entre las propiedades tenemos: nombre, tipo de dato,
        /// longitud, y si acepta nulos.
        /// </summary>
        public class Columnas
        {
            public string Nombre { get; set; }
            public string TipoDato { get; set; }
            public int Indice { get; set; }
            public bool EsNulo { get; set; }
            public string LongitudMaxima { get; set; }

            protected string MaxLengthFormatted => LongitudMaxima.Equals("-1") ? "max" : LongitudMaxima;

            /// <summary>
            /// Función para obtener datos del reader y llenar las propiedades de una columna: nombre, tipo de dato, longitud y si acepta nulos
            /// </summary>
            /// <param name="reader"></param>
            /// <returns></returns>
            public Columnas ReadFromReader(IDataReader reader)
            {
                //Información de las columnas, nombre, tipo de datos, posición, es nulo y longitud en caso de ser tipo caracter
                this.Nombre = reader["COLUMN_NAME"].ToString();
                this.TipoDato = reader["DATA_TYPE"].ToString();
                this.Indice = (int)reader["ORDINAL_POSITION"];
                this.EsNulo = ((string)reader["IS_NULLABLE"]) == "YES";
                this.LongitudMaxima = reader["CHARACTER_MAXIMUM_LENGTH"].ToString();

                return this;
            }

            /// <summary>
            /// Función para regresar el formato de una columna con su nombre, tipo de dato, longitud y si acepta nulos.
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return string.Format("[{0}] {1}{2} {3}NULL", Nombre, TipoDato,
                    LongitudMaxima == string.Empty ? "" : "(" + MaxLengthFormatted + ")",
                    EsNulo ? "" : "NOT ");
            }

        }

        /// <summary>
        /// Función para obtener la llave primaria de una tabla, y generar los filtros necesarios para obtener
        /// sólo los registros que se van a restaurar
        /// </summary>
        /// <param name="tabla"></param>
        /// <returns></returns>
        public static string LlavePrimaria(string tabla)
        {
            var filtro = string.Empty;
            var nulo = string.Empty;

            var consulta =
                "SELECT ColumnName = col.column_name FROM information_schema.table_constraints tc " +
                "INNER JOIN information_schema.key_column_usage col " +
                "ON col.Constraint_Name = tc.Constraint_Name " +
                "AND col.Constraint_schema = tc.Constraint_schema " +
                "WHERE tc.Constraint_Type = 'Primary Key' AND col.Table_name = '" + tabla + "'";

            var ds = Consulta(consulta);
            if (ds.Tables[0].Rows.Count <= 0)
                throw new Exception("Tabla no tiene llave primaria !!!");
            else
            {
                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        nulo = " AND b." + ds.Tables[0].Rows[i][0] + " is null";

                        filtro = " b ON a." + ds.Tables[0].Rows[i][0] + " = b." + ds.Tables[0].Rows[i][0];
                    }
                    else
                        filtro = filtro + " AND a." + ds.Tables[0].Rows[i][0] + " = b." + ds.Tables[0].Rows[i][0];
                }
            }

            return filtro + "," + nulo;
        }

        /// <summary>
        /// Restaurar datos a una tabla mediante de un archivo csv mediante un BulkCopy
        /// </summary>
        /// <param name="csvFileData"></param>
        /// <param name="tablaRestaurar"></param>
        public static void RestaurarDatosTabla(DataTable csvFileData, string tablaRestaurar)
        {
            //var conn = ObtenerConexion();            
            using (var volcado = new SqlBulkCopy(ObtieneStringConexion(), SqlBulkCopyOptions.KeepIdentity))
            {
                volcado.DestinationTableName = tablaRestaurar;
                foreach (var column in csvFileData.Columns)
                    volcado.ColumnMappings.Add(column.ToString(), column.ToString());
                volcado.WriteToServer(csvFileData);
            }
        }

        /// <summary>
        /// Función para respaldar una tabla para un campo y registros (llaves) elegidos. Se pueden
        /// eliminar los registros de la tabla y se genera un archivo con estructura csv.
        /// </summary>
        /// <param name="tablaRespaldar"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="tipoCampo"></param>
        /// <param name="llavesRespaldar"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="limpiarRegistros"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="guardarTitulos"></param>
        public static void _respaldador(string tablaRespaldar, string campoFiltrado, string tipoCampo, string llavesRespaldar,
            string rutaArchivo, string nombreArchivo, bool limpiarRegistros, bool sobreescribirarchivo, bool guardarTitulos)
        {
            if (TablaExiste(tablaRespaldar))
            {
                var columnas = ObtieneColumnas(ObtieneId(tablaRespaldar));

                //Si hay llaves a respaldar
                if (string.IsNullOrEmpty(llavesRespaldar)) return;

                //Valida Filtro
                var mensaje = _validaFiltro(llavesRespaldar, tipoCampo);

                var _where = string.Empty;
                if (mensaje == 0)
                    _where = GeneraWhere(campoFiltrado, tipoCampo, llavesRespaldar, false);

                //Si existen filtros se realiza CSV
                if (!string.IsNullOrEmpty(_where))
                {
                    var dt = Consulta("Select " + columnas + " from " + tablaRespaldar + _where).Tables[0];
                    _generaCSV(dt, rutaArchivo, nombreArchivo, sobreescribirarchivo, guardarTitulos);

                    //Variable para saber si se eliminan registros de la tabla
                    if (limpiarRegistros)
                        EliminaDatos("DELETE from " + tablaRespaldar + _where);
                }
                else
                {
                    //Se encontro un error al generar los filtros
                    var sb = new StringBuilder();
                    sb.AppendLine(_obtenerDescripcionEnum((mensajes)mensaje));
                    _generaArchivo(rutaArchivo, nombreArchivo, sb);
                    throw new Exception(_obtenerDescripcionEnum((mensajes)mensaje));
                }
            }
            else
            {
                //Error Tabla no Existe
                var sb = new StringBuilder();
                sb.AppendLine(_obtenerDescripcionEnum((mensajes)2));
                _generaArchivo(rutaArchivo, nombreArchivo, sb);
                throw new Exception(_obtenerDescripcionEnum((mensajes)2));
            }

        }

        /// <summary>
        /// Función para validar si existe una tabla
        /// </summary>
        /// <param name="nombreTabla"></param>
        /// <returns></returns>
        public static bool TablaExiste(string nombreTabla)
        {
            var existe = false;

            var ds = Consulta("select 1 from sys.objects where name = '" + nombreTabla + "'");

            if (ds.Tables[0].Rows.Count > 0)
                existe = true;

            return existe;
        }

        /// <summary>
        /// Función para obtener los nombres y id's separados por comas de las columnas de una
        /// tabla. Los nombres y id's se obtienen filtrando por el id (object_id) de una tabla.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ObtieneColumnas(int id)
        {
            var columnas = string.Empty;
            var ds = Consulta("select name, column_id from sys.columns where object_id = '" + id + "'");
            if (ds.Tables[0].Rows.Count <= 0) return columnas.TrimEnd(',');
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                columnas = columnas + row[0] + ",";
            }
            return columnas.TrimEnd(',');
        }

        /// <summary>
        /// Función para obtener id de una tabla, filtrando por el nombre de la tabla.
        /// </summary>
        /// <param name="nombreTabla"></param>
        /// <returns></returns>
        public static int ObtieneId(string nombreTabla)
        {
            var id = 0;

            var ds = Consulta("select object_Id from sys.objects where name = '" + nombreTabla + "'");

            if (ds.Tables[0].Rows.Count > 0)
                id = int.Parse(ds.Tables[0].Rows[0][0].ToString());

            return id;
        }

        /// <summary>
        /// Función para validar los tipos de datos númericos, string y fecha
        /// </summary>
        /// <param name="llaves"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static int _validaFiltro(string llaves, string tipo)
        {
            var mensaje = 0;

            /* Tipos de Datos */
            switch (tipo.ToLower())
            {
                /* Numericos Exactos sin Decimales */
                case "bigint":
                case "bit":
                case "int":
                case "smallint":
                case "tinyint":
                    mensaje = _validaDatosNumericosSinDecimales(llaves);
                    break;
                /* Numericos Exactos con Decimales */
                case "decimal":
                case "money":
                case "numeric":
                case "smallmoney":
                /* Numericos Aproximados */
                case "float":
                case "real":
                    mensaje = _validaDatosNumericosConDecimales(llaves);
                    break;
                /* Fecha y Hora*/
                case "date":
                case "smalldatetime":
                case "datetime":
                    mensaje = _validaDatosFecha(llaves);
                    break;
                /* Strings */
                case "char":
                case "varchar":
                case "text":
                    mensaje = _validaDatosString(llaves);
                    break;
                default:
                    mensaje = (int)mensajes.TipoDatoNoValido;
                    break;
            }

            return mensaje;
        }

        /// <summary>
        /// Función para validar que los datos númericos a filtrar son 
        /// de tipo númerico sin decimales, se pueden manejar rangos 
        /// separados por el símbolo - y se pueden separar elementos mediante
        /// comas, solo se permite capturar elementos de tipo númerico.
        /// Cualquier error se escribe en el archivo de salida.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static int _validaDatosNumericosSinDecimales(string llaves)
        {
            var mensaje = 0;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Any(numero => !char.IsDigit(numero)))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }

                if (data2.Any(numero => !char.IsDigit(numero)))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }
            }

            return mensaje;
        }

        /// <summary>
        /// Función para validar que los datos o llaves a respaldar correspondan
        /// a los datos númericos con decimales, permite manejo de rangos separados 
        /// por el símbolo - y separar elementos por comas, y cada elemento permite 
        /// sólo un punto decimal y sólo se deben capturar números. Cualquier error
        /// se escribe sobre el archivo de salida el tipo de error que se genero.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static int _validaDatosNumericosConDecimales(string llaves)
        {
            var mensaje = 0;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Count(f => f == '.') > 1)
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }
                if (data1.Any(numero => !char.IsNumber(numero) && numero.ToString() != "."))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }

                if (data2.Count(f => f == '.') > 1)
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }
                if (data2.Any(numero => !char.IsDigit(numero) && numero.ToString() != "."))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }
            }

            return mensaje;
        }

        /// <summary>
        /// Función para validar que los datos o llaves a respaldar correspondan
        /// al tipo de datos fecha, maneja rangos separados por el símbolo - y comas 
        /// para separar las fechas, además solo permite números. 
        /// Ejemplo: 20170101-20170131,20170201 
        /// Los errores se escriben en el archivo de salida.
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static int _validaDatosFecha(string llaves)
        {
            var mensaje = 0;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                //valida tamaño data1
                if (data1.Length != 8 || data2.Length != 8)
                {
                    mensaje = (int)mensajes.FechaNoValida;
                    break;
                }

                //Valida que fecha contenga solo digitos
                if (data1.Any(numero => !char.IsNumber(numero)) || data2.Any(numero => !char.IsNumber(numero)))
                {
                    mensaje = (int)mensajes.FechaNoValida;
                    break;
                }

                //valida mes
                if (!ValidaMes(data1.Substring(4, 2)) || !ValidaMes(data2.Substring(4, 2)))
                {
                    mensaje = (int)mensajes.MesNoValido;
                    break;
                }

                //valida dia
                if (ValidaDia(data1) && ValidaDia(data2)) continue;
                mensaje = (int)mensajes.DiaNoValido;
                break;
            }

            return mensaje;
        }


        /// <summary>
        /// Función para valdiar el mes, entre 1 y 12.
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        public static bool ValidaMes(string mes)
        {
            var valido = (int.Parse(mes) > 1 && int.Parse(mes) < 12);
            return valido;
        }

        /// <summary>
        /// Función para validar el día, toma como entrada la fecha en formato universal yyyyMMdd.
        /// Verifica si el año es bisiesto, y si el número de día es valido para el mes, ya sea
        /// 30, 31, 28 o 29 según el mes.
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static bool ValidaDia(string fecha)
        {
            var valido = false;
            var year = fecha.Substring(0, 4);
            var mes = fecha.Substring(4, 2);
            var dia = fecha.Substring(6, 2);
            var bisiesto = DateTime.IsLeapYear(int.Parse(year)) ? 29 : 28;

            switch (int.Parse(mes))
            {
                //31 Días
                case 1: //Enero
                case 3: //Marzo                
                case 5: //Mayo                
                case 7: //Julio
                case 8: //Agosto
                case 10: //Octubre
                case 12: //Diciembre
                    if (int.Parse(dia) >= 1 || int.Parse(dia) <= 31)
                        valido = true;
                    break;
                case 4: //Abril
                case 6: //Junio
                case 9: //Septiembe
                case 11: //Noviembre
                    if (int.Parse(dia) >= 1 || int.Parse(dia) <= 30)
                        valido = true;
                    break;
                case 2: //Febrero
                    if ((int.Parse(dia) >= 1 || int.Parse(dia) <= bisiesto))
                        valido = true;
                    break;
                default:
                    valido = false;
                    break;
            }

            return valido;
        }

        /// <summary>
        /// Función para validar datos tipo texto
        /// </summary>
        /// <param name="llaves"></param>
        /// <returns></returns>
        public static int _validaDatosString(string llaves)
        {
            var mensaje = 0;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                if (data1 == null) throw new ArgumentNullException(nameof(data1));
                var data2 = item.Split('-').LastOrDefault();
                if (data2 == null) throw new ArgumentNullException(nameof(data2));

                if (data1.Any(numero => !char.IsLetterOrDigit(numero)))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }

                if (data2.Any(numero => !char.IsLetterOrDigit(numero)))
                {
                    mensaje = (int)mensajes.LlaveNoValida;
                    break;
                }
            }

            return mensaje;
        }

        /// <summary>
        /// Función para obtener el filtro de cualquier tipo de dato, ya 
        /// sea númerico, fecha o de tipo string. La función genera una
        /// cadena o string con el filtro generado tomando en cuenta el 
        /// tipo de dato y las llaves o registros a filtrar.
        /// </summary>
        /// <param name="campoFiltrado"></param>
        /// <param name="tipo"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string GeneraWhere(string campoFiltrado, string tipo, string llaves, bool alias)
        {
            string filtro;

            /* Tipo de Datos */
            switch (tipo.ToLower())
            {
                /* Numericos Exactos sin Decimales */
                case "bigint":
                case "bit":
                case "int":
                case "smallint":
                case "tinyint":
                /* Numericos Exactos con Decimales */
                case "decimal":
                case "money":
                case "numeric":
                case "smallmoney":
                /*** Numericos Aproximados ***/
                case "float":
                case "real":
                    filtro = ObtenerFiltro(tipo, campoFiltrado, llaves, alias);
                    break;
                /*** Fecha y Hora ***/
                case "date":
                case "smalldatetime":
                case "datetime":
                    filtro = _obtenerFiltroDatosFecha(tipo, campoFiltrado, llaves, alias);
                    break;
                /* Strings */
                case "char":
                case "varchar":
                case "text":
                    filtro = ObtenerFiltro(tipo, campoFiltrado, llaves, alias);
                    break;
                default:
                    // Se pasa vacío ya que la función _validaFiltro hace la tarea de 
                    // escribir el mensaje de error, por tanto no es necesario, un manejo
                    // en esta parte, pero se agrega el default por consistencia.
                    filtro = string.Empty;
                    break;
            }

            return filtro;
        }

        /// <summary>
        /// Función para generar el filtro.
        /// </summary>
        /// <param name="tipoDato"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string ObtenerFiltro(string tipoDato, string campoFiltrado, string llaves, bool alias)
        {
            var filtro = string.Empty;

            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                var data2 = item.Split('-').LastOrDefault();

                if (alias)
                    campoFiltrado = "a." + campoFiltrado;

                if (string.IsNullOrEmpty(filtro))
                {
                    if (data1 == data2)
                        filtro = " WHERE " + campoFiltrado + " = " + TextoConComillas(tipoDato, data1);
                    else
                        filtro = " WHERE " + campoFiltrado + " BETWEEN " + TextoConComillas(tipoDato, data1)
                            + " AND " + TextoConComillas(tipoDato, data2);
                }
                else
                {
                    if (data1 == data2)
                    {
                        filtro = filtro + " OR " + campoFiltrado + " = " + TextoConComillas(tipoDato, data1);
                    }
                    else
                        filtro = filtro + " OR " + campoFiltrado + " BETWEEN " + TextoConComillas(tipoDato, data1)
                            + " AND " + TextoConComillas(tipoDato, data2);
                }
            }

            return filtro;
        }

        /// <summary>
        /// Función para validar si el tipo es texto lo regresa entre comillas.
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string TextoConComillas(string tipo, string data)
        {
            string concomillas;

            switch (tipo.ToLower())
            {
                case "char":
                case "varchar":
                case "text":
                    concomillas = "'" + data + "'";
                    break;
                default:
                    concomillas = data;
                    break;
            }

            return concomillas;
        }


        /// <summary>
        /// Función para generar el filtro para los tipos de datos fecha. Smalldatetime y datetime
        /// se convierte a Date, ya que solo se filtra por fecha.
        /// </summary>
        /// <param name="tipoDato"></param>
        /// <param name="campoFiltrado"></param>
        /// <param name="llaves"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static string _obtenerFiltroDatosFecha(string tipoDato, string campoFiltrado, string llaves, bool alias)
        {
            var filtro = string.Empty;
            foreach (var item in llaves.Split(','))
            {
                var data1 = item.Split('-').FirstOrDefault();
                var data2 = item.Split('-').LastOrDefault();

                if (alias)
                    campoFiltrado = "a." + campoFiltrado;

                if (string.IsNullOrEmpty(filtro))
                {
                    if (data1 == data2)
                        filtro = " WHERE CONVERT(DATE, " + campoFiltrado + ") = CONVERT(DATE, '" + data1 + "')";
                    else
                        filtro = " WHERE CONVERT(DATE, " + campoFiltrado + ") BETWEEN CONVERT(DATE, '" + data1 + "')"
                            + " AND CONVERT(" + tipoDato + ", '" + data2 + "')";
                }
                else
                {
                    if (data1 == data2)
                        filtro = filtro + " OR " + campoFiltrado + " = CONVERT(DATE, '" + data1 + "')";
                    else
                        filtro = filtro + " OR " + campoFiltrado + " BETWEEN CONVERT(DATE, '" + data1 + "')"
                            + " AND CONVERT(DATE, '" + data2 + "')";
                }
            }

            return filtro;
        }

        /// <summary>
        /// Función para generar archivo CSV, encabezados y registros, así como guardar el archivo
        /// en una ruta dada. Los encabezados (títulos) y sobreescritura del archivo son parámetros
        /// que pueden o no incluir los títulos o sobreescritura del archivo, si no se permite la
        /// sobreescritura genera una versión del archivo con un nombre sugerido.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="guardarTitulos"></param>
        public static void _generaCSV(DataTable dt, string rutaArchivo, string nombreArchivo, bool sobreescribirarchivo, bool guardarTitulos)
        {
            var sb = new StringBuilder();

            //Columnas del archivo csv
            var columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);//IEnumerable<string>

            if (guardarTitulos)
            {
                //Se agregan los títulos o nombres de las columnas
                sb.AppendLine(string.Join(",", columnNames));
            }

            //Datos que arroja la consulta
            foreach (DataRow row in dt.Rows)
            {
                //IEnumerable<string>
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            //Genera archivo
            GenerarArchivoZip(sb, rutaArchivo, sobreescribirarchivo, nombreArchivo, true);
        }

        /// <summary>
        /// Función para generar archivo zip y genera el archivo. Si el parámetro sobreescribirarchivo
        /// esta con valor falso, no se genera el archivo y se genera un mensaje.
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="rutaArchivo"></param>
        /// <param name="sobreescribirarchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="tipoZip"></param>
        public static void GenerarArchivoZip(StringBuilder sb, string rutaArchivo, bool sobreescribirarchivo, string nombreArchivo, bool tipoZip)
        {
            //Si no se sobreescribe y archivo existe
            var nombreZip = Path.ChangeExtension(nombreArchivo, "zip");
            if (nombreZip != null)
            {
                var nombreZipRuta = Path.Combine(rutaArchivo, nombreZip);

                if (File.Exists(nombreZipRuta))
                {
                    if (!sobreescribirarchivo)
                    {
                        var sbM = new StringBuilder();
                        sbM.AppendLine(_obtenerDescripcionEnum((mensajes)7)); // Mensaje Archivo ya existe
                        _generaArchivo(rutaArchivo, nombreArchivo, sbM);
                        throw new Exception(_obtenerDescripcionEnum((mensajes)7));
                    }
                }
            }

            GeneraZip(rutaArchivo, nombreArchivo, sb);
        }

        /// <summary>
        /// Función para genera archivo zip
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sb"></param>
        public static void GeneraZip(string rutaArchivo, string nombreArchivo, StringBuilder sb)
        {
            try
            {
                //Si no existe la ruta, intenta crear el directorio
                if (rutaArchivo != null && !Directory.Exists(rutaArchivo))
                    Directory.CreateDirectory(rutaArchivo);

                // Generar el archivo en formato zip y dentro el archivo con el nombre de archivo capturado
                var zip = new ZipFile();
                if (rutaArchivo == null) return;
                zip.AddEntry(Path.Combine(rutaArchivo, nombreArchivo), sb.ToString(), Encoding.UTF8);
                zip.Save(Path.Combine(rutaArchivo, Path.ChangeExtension(nombreArchivo, "zip"))); // Extension debería ser Zip
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error al generar archivo Zip. {0}", e);
            }
        }

        /// <summary>
        /// Función para genera archivo
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <param name="nombreArchivo"></param>
        /// <param name="sb"></param>
        public static void _generaArchivo(string rutaArchivo, string nombreArchivo, StringBuilder sb)
        {
            try
            {
                //Si no existe la ruta, intenta crear el directorio
                if (rutaArchivo != null && !Directory.Exists(rutaArchivo))
                    Directory.CreateDirectory(rutaArchivo);

                File.WriteAllText(Path.Combine(rutaArchivo, nombreArchivo), sb.ToString());
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error al generar archivo. {0}", new Exception(e.Message));
            }
        }

        /// <summary>
        /// Función que ejecuta una consulta en SQL
        /// </summary>
        /// <param name="consulta"></param>
        /// <returns></returns>
        public static DataSet Consulta(string consulta)
        {
            var ds = new DataSet();
            var dt = new DataTable();
            var conn = ObtenerConexion();
            var comando = new SqlCommand(consulta, conn);
            dt.Load(comando.ExecuteReader());
            ds.Tables.Add(dt);
            conn.Close();

            return ds;
        }

        /// <summary>
        /// Función para eliminar registros de una tabla
        /// </summary>
        /// <param name="elimina"></param>
        public static void EliminaDatos(string elimina)
        {
            var conn = ObtenerConexion();
            var comandoElimina = new SqlCommand(elimina, conn);
            comandoElimina.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Función para establecer una conexión a SQL
        /// </summary>
        /// <returns></returns>
        public static SqlConnection ObtenerConexion()
        {
            try
            {
                var conexion = new SqlConnection(ObtieneStringConexion());
                conexion.Open();
                return conexion;
            }
            catch (NullReferenceException)
            {
                throw new ApplicationException("Base de datos no definida");
            }
            catch (FormatException)
            {
                throw new ApplicationException("Error en la definición de la base de datos");
            }
            catch
            {
                throw new ApplicationException("No se pudo conectar a la base de datos.");
            }
        }

        /// <summary>
        /// Función para obtener string de conexión en archivo de configuración
        /// </summary>
        /// <returns></returns>
        public static string ObtieneStringConexion()
        {
            return ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        }

        /// <summary>
        /// Función para obtener la descripción de un enum, útil para desplegar los mensajes de error,
        /// En el archivo de salida.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string _obtenerDescripcionEnum(Enum value)
        {
            // Get the Description attribute value for the enum value
            var fi = value.GetType().GetField(value.ToString());
            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

    }/**/
}
