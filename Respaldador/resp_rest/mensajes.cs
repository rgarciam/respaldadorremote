﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace respaldador
{
    public enum mensajes
    {
        [Description("Llave Valida")]
        LlaveValida = 0,
        [Description("Llave no valida para el tipo de dato")]
        LlaveNoValida = 1,
        [Description("Tabla No Existe")]
        TablaNoExiste = 2,
        [Description("Fecha No Valida")]
        FechaNoValida = 3,
        [Description("Mes No Valido")]
        MesNoValido = 4,
        [Description("Día No Valido")]
        DiaNoValido = 5,
        [Description("Tipo de Dato No Valido")]
        TipoDatoNoValido = 6,
        [Description("Archivo ya existe, habilite sobreescritura")]
        ArchivoYaExiste = 7
    }
}